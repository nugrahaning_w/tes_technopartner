<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['category_parent_id','category_name','category_description', 'created_at', 'updated_at'];
}
