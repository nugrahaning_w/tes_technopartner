<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $data_category = \App\Category::where('category_parent_id', '>=', 1)->get();
        $data_parent = \App\Category::where('category_parent_id', '=', 0)->get();
        $data = [
            'data_category' => $data_category,
            'data_parent' => $data_parent
        ];
        return view('category.index', $data);
    }
    public function create(Request $request)
    {
        $category_parent_id = $request->in_type_category;
        $category_name = $request->in_category_name;
        $category_description = $request->in_category_description;

        if ($category_parent_id == null){
            return redirect('/category')->with('gagal', 'Anda wajib mengisi tipe Kategori!');
        }else{
            $data = [
                'category_parent_id' => $category_parent_id,
                'category_name' => $category_name,
                'category_description' => $category_description
            ];
    
            \App\Category::create($data);
            return redirect('/category')->with('sukses', 'Data berhasil ditambahkan!');
        }
    }
    public function edit($id)
    {
        $data_selected = \App\Category::where('id', '=', $id)->get();
        $data_parent = \App\Category::where('category_parent_id', '=', 0)->get();
        $data = [
            'data_selected' => $data_selected,
            'data_parent' => $data_parent
        ];
        // return $data;
        return view('category.edit', $data);
    }
    public function update($id, Request $request)
    {
        $category = \App\Category::find($id);
        
        $category_parent_id = $request->in_type_category;
        $category_name = $request->in_category_name;
        $category_description = $request->in_category_description;
        $updated_at = now();
        
        $data = [
            'category_parent_id' => $category_parent_id,
            'category_name' => $category_name,
            'category_description' => $category_description,
            'updated_at' => $updated_at
        ];
    
        $category->update($data);
        
        return redirect('/category')->with('sukses', 'Data berhasil dirubah!');
        
    }

    public function delete($id)
    {
        $category = \App\Category::find($id);
        $category->delete($category);

        return redirect('/category')->with('sukses', 'Data berhasil dihapus!');
        
    }
}
