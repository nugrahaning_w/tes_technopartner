<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        $data_transaksi = \App\Transaction::all();
        $data_category = \App\Category::where('category_parent_id', '>=', 1)->get();
        $data = [
            'data_category' => $data_category,
            'data_transaksi' => $data_transaksi
        ];
        return view('transaction.index', $data);
    }
    public function create(Request $request)
    {
        $type = \App\Category::where('id', '=', $request->in_transaksi_category)->get();
        $type_id = $type[0]->category_parent_id;
        $transaction_category = $request->in_transaksi_category;
        $transaction_name = $request->in_transaksi_name;
        $transaction_description = $request->in_transaksi_description;
        $transaction_amount = $request->in_transaksi_amount;
            
            $data = [
                'category_id' => $transaction_category,
                'transaction_name' => $transaction_name,
                'transaction_description' => $transaction_description,
                'transaction_amount' => $transaction_amount,
                'transaction_type' => $type_id,
                'created_at' => now(),
                'updated_at' => now()
            ];
    
        \App\Transaction::create($data);
        return redirect('/transaction')->with('sukses', 'Data berhasil ditambahkan!');
        
    }
    public function edit($id)
    {
        $data_category = \App\Category::all();
        $data_transaksi = \App\Transaction::where('id', '=', $id)->get();
        $data = [
            'data_selected' => $data_transaksi,
            'data_category' => $data_category
        ];
        // return $data;
        return view('transaction.edit', $data);
    }
    public function update($id, Request $request)
    {
        $transaksi = \App\Transaction::find($id);
        $type = \App\Category::where('id', '=', $request->in_transaksi_category)->get();
        $type_id = $type[0]->category_parent_id;
        $transaction_category = $request->in_transaksi_category;
        $transaction_name = $request->in_transaksi_name;
        $transaction_description = $request->in_transaksi_description;
        $transaction_amount = $request->in_transaksi_amount;
            
            $data = [
                'category_id' => $transaction_category,
                'transaction_name' => $transaction_name,
                'transaction_description' => $transaction_description,
                'transaction_amount' => $transaction_amount,
                'transaction_type' => $type_id,
                'updated_at' => now()
            ];
        
        $transaksi->update($data);
        
        return redirect('/transaction')->with('sukses', 'Data berhasil dirubah!');
        
    }

    public function delete($id)
    {
        $transaksi = \App\Transaction::find($id);
        $transaksi->delete($transaksi);

        return redirect('/category')->with('sukses', 'Data berhasil dihapus!');
        
    }
}
