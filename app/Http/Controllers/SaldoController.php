<?php

namespace App\Http\Controllers;

class SaldoController extends Controller
{
    public function index()
    {
        $saldo = 0;
        $data_pemasukan = \App\Transaction::where('transaction_type', '=', 1)->get();
        $data_pengeluaran = \App\Transaction::where('transaction_type', '=', 2)->get();
        foreach ($data_pemasukan as $in) {
            $saldo = $saldo + $in->transaction_amount;
        }
        foreach ($data_pengeluaran as $out) {
            $saldo = $saldo - $out->transaction_amount;
        }
        $data = [
            'saldo' => $saldo
        ];
        
        return view('index', $data);
    }
}
