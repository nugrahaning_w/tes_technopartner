<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = ['category_id','transaction_name', 'transaction_type', 'transaction_description', 'transaction_amount', 'created_at', 'updated_at'];
}
