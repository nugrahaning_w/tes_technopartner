<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parents = [
            ['category_parent_id' => 0,
            'category_name' => 'Pemasukan',
            'category_description' => 'Category parent untuk semua pemasukan',
            'created_at' => now(),
            'updated_at' => now(),
            ],
            ['category_parent_id' => 0,
            'category_name' => 'Pengeluaran',
            'category_description' => 'Category parent untuk semua pengeluaran',
            'created_at' => now(),
            'updated_at' => now(),
            ],
        ];
        foreach($parents as $parents){
            DB::table('category')->insert($parents);
        }
    }
}
