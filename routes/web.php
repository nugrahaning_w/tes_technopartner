<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// category
Route::get('/category', 'CategoryController@index');
Route::post('/category/create', 'CategoryController@create');
Route::get('/category/edit/{id}', 'CategoryController@edit');
Route::get('/category/delete/{id}', 'CategoryController@delete');
Route::post('/category/update/{id}', 'CategoryController@update');
// transaksi
Route::get('/transaction', 'TransactionController@index');
Route::post('/transaction/create', 'TransactionController@create');
Route::get('/transaction/edit/{id}', 'TransactionController@edit');
Route::get('/transaction/delete/{id}', 'TransactionController@delete');
Route::post('/transaction/update/{id}', 'TransactionController@update');
// Saldo
Route::get('/', 'SaldoController@index');

