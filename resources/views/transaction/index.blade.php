@extends('template.menu')
@section('content')
        <div class="col-6">
            <h2 class="align-content-center">Table Transaksi</h2>
        </div>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        @if(session('gagal'))
        <div class="alert alert-danger" role="alert">
            {{session('gagal')}}
        </div>
        @endif
        
        <div>
            <table class = "table">
                <tr>
                    <th>Tipe Transaksi</th>
                    <th>Nama Transaksi</th>
                    <th>Deskripsi Transaksi</th>
                    <th>Nominal Transaksi</th>
                    <th>Tanggal Transaksi</th>
                    <th>Action</th>
                </tr>
                @foreach($data_transaksi as $transaksi)
                <tr>
                    <td><?php if ($transaksi->transaction_type == 1) { echo "Pemasukan";} else { echo "Pengeluaran";}?></td>
                    <td>{{$transaksi->transaction_name}}</td>
                    <td>{{$transaksi->transaction_description}}</td>
                    <td>{{$transaksi->transaction_amount}}</td>
                    <td>{{$transaksi->updated_at}}</td>
                    <td>
                        <a href="/transaction/delete/{{$transaksi->id}}" class="btn btn-danger">Hapus</a>
                        <a href="/transaction/edit/{{$transaksi->id}}" class="btn btn-secondary" >Edit</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="col-6">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Tambah Transaksi
            </button>

            <!-- Modal tambah-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action = "/transaction/create" method="POST">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="type_category">Kategori Transaksi</label>
                            <select name="in_transaksi_category" class="form-control" id="in_type_category">
                                <option value="" selected = "selected">Pilih Tipe Kategori</option>
                                @foreach($data_category as $category)
                                    <option value = "<?php echo $category->id?>">{{$category->category_name}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category_name">Nama Transaksi</label>
                            <input required = "required" name = "in_transaksi_name" type="text" class="form-control" id="in_category_name">
                        </div>
                        <div class="form-group">
                            <label for="category_name">Jumlah Transaksi</label>
                            <input required = "required" name = "in_transaksi_amount" type="number" class="form-control" id="in_category_name" placeholder="99999" min=0>
                        </div>
                        <div class="form-group">
                            <label for="category_description">Diskripsi Transaksi</label>
                            <textarea required = "required" name = "in_transaksi_description" class="form-control" id="in_category_description" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    </div>
                </div>
            </div>
            </div>
            <!-- end modal tambah -->
        </div>
@endsection