@extends('template.menu')
@section('content')
<div class="col-6">
    <h2 class="align-content-center">Edit Transaksi</h2>
</div>
<form action = "/transaction/update/{{$data_selected[0]->id}}" method="POST">
    {{ csrf_field() }}
        <div class="form-group">
            <label for="type_category">Kategori Transaksi</label>
            <select name="in_transaksi_category" class="form-control" id="in_type_category">
                @foreach($data_category as $category)
                <?php if ($category->id == $data_selected[0]->category_id){?>
                    <option selected = "selected" value = "<?php echo $category->id?>">{{$category->category_name}}</option><?php ;} else{?>
                    <option value = "<?php echo $category->id?>">{{$category->category_name}}</option>
                <?php ;} ?>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="transaksi_name">Nama Transaksi</label>
            <input required = "required" value = "{{$data_selected[0]->transaction_name}}" name = "in_transaksi_name" type="text" class="form-control" id="in_transaksi_name">
        </div>
        <div class="form-group">
            <label for="transaksi_amount">Jumlah Transaksi</label>
            <input required = "required" value = "{{$data_selected[0]->transaction_amount}}" name = "in_transaksi_amount" type="number" class="form-control" id="in_transaksi_name">
        </div>
        <div class="form-group">
            <label for="transaksi_description">Diskripsi Transaksi</label>
            <textarea required = "required" name = "in_transaksi_description" class="form-control" id="in_category_description" rows="3">{{$data_selected[0]->transaction_description}}</textarea>
        </div>
    
    <a href="/transaction" class="btn btn-secondary" data-dismiss="modal">Back</a>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection