@extends('template.menu')
@section('content')
        <div class="col-6">
            <h2 class="align-content-center">Table Category</h2>
        </div>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        @if(session('gagal'))
        <div class="alert alert-danger" role="alert">
            {{session('gagal')}}
        </div>
        @endif
        
        <div>
            <table class = "table">
                <tr>
                    <th>Nama Kategori</th>
                    <th>Deskripsi Kategori</th>
                    <th>Tipe Kategori</th>
                    <th>Action</th>
                </tr>
                @foreach($data_category as $category)
                <tr>
                    <td>{{$category->category_name}}</td>
                    <td>{{$category->category_description}}</td>
                    <td><?php if ($category->category_parent_id == 1) { echo "Pemasukan";} else { echo "Pengeluaran";}?></td>
                    <td>
                        <a href="/category/delete/{{$category->id}}" class="btn btn-danger">Hapus</a>
                        <a href="/category/edit/{{$category->id}}" class="btn btn-secondary" >Edit</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="col-6">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Tambah Kategori
            </button>

            <!-- Modal tambah-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action = "/category/create" method="POST">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="type_category">Tipe Kategori</label>
                            <select name="in_type_category" class="form-control" id="in_type_category">
                                <option value="" selected = "selected">Pilih Tipe Kategori</option>
                                @foreach($data_parent as $parent)
                                    <option value = "<?php echo $parent->id?>">{{$parent->category_name}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category_name">Nama Kategory</label>
                            <input required = "required" name = "in_category_name" type="text" class="form-control" id="in_category_name">
                        </div>
                        <div class="form-group">
                            <label for="category_description">Diskripsi Kategory</label>
                            <textarea required = "required" name = "in_category_description" class="form-control" id="in_category_description" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    </div>
                </div>
            </div>
            </div>
            <!-- end modal tambah -->
        </div>
@endsection