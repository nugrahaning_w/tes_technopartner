@extends('template.menu')
@section('content')
<div class="col-6">
    <h2 class="align-content-center">Edit Category</h2>
</div>
<form action = "/category/update/{{$data_selected[0]->id}}" method="POST">
    {{ csrf_field() }}
        <div class="form-group">
            <label for="type_category">Tipe Kategori</label>
            <select name="in_type_category" class="form-control" id="in_type_category">
                @foreach($data_parent as $parent)
                <?php if ($parent->id == $data_selected[0]->category_parent_id){?>
                    <option selected = "selected" value = "<?php echo $parent->id?>">{{$parent->category_name}}</option><?php ;} else{?>
                    <option value = "<?php echo $parent->id?>">{{$parent->category_name}}</option>
                <?php ;} ?>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="category_name">Nama Kategori</label>
            <input required = "required" value = "{{$data_selected[0]->category_name}}" name = "in_category_name" type="text" class="form-control" id="in_category_name">
        </div>
        <div class="form-group">
            <label for="category_description">Diskripsi Kategory</label>
            <textarea required = "required" name = "in_category_description" class="form-control" id="in_category_description" rows="3">{{$data_selected[0]->category_description}}</textarea>
        </div>
    
    <a href="/category" class="btn btn-secondary" data-dismiss="modal">Back</a>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection